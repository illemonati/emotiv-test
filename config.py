import os
import json

user = {}

try:
    with open("./secret.json") as secret:
        user = json.load(secret)
except Exception as e:
    pass


try:
    user = {
        "client_id": os.environ["CLIENT_ID"],
        "client_secret": os.environ["CLIENT_SECRET"],
        "license": os.environ["LICENSE"],
        "debit": 1
    }
except Exception as e:
    pass
