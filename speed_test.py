from cortex import Cortex
from config import user
import json
import time
import numpy as np


def main():
    client = Cortex(user, debug_mode=True)
    client.do_prepare_steps()
    client.sub_request(['eeg'])
    meta_data = client.ws.recv()
    last_time, current_time = time.time(), time.time()
    times = np.zeros(1000)
    data_arr = []
    for i in range(1000):
        last_time = current_time
        data = json.loads(client.ws.recv())
        data_arr.append(data)
        current_time = time.time()
        times[i] = current_time - last_time

    print(np.mean(1.0/times))


if __name__ == '__main__':
    main()
