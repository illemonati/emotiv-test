from cortex import Cortex
from config import user
import json
import time
import numpy as np

SECONDS = 8


def main():
    client = Cortex(user, debug_mode=True)
    client.do_prepare_steps()
    client.sub_request(['eeg'])
    meta_data = client.ws.recv()
    data_arr = []
    data = json.loads(client.ws.recv())
    print(data)
    start_time = data["time"]
    end_time = 0
    recv_end_time = 0
    datas = []
    while True:
        data = json.loads(client.ws.recv())
        datas.append(data)
        print(data)
        timestamp = data["time"]
        if timestamp >= start_time + 8:
            end_time = timestamp
            recv_end_time = time.time()
            break

    print(end_time - start_time)
    print(recv_end_time - start_time)
    print(recv_end_time - end_time)
    print(len(datas))
    print(len(datas)/SECONDS)


if __name__ == '__main__':
    main()
