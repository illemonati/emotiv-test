from cortex import Cortex
from config import user
import json
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation


class Grapher:
    def __init__(self, ws, cols_to_graph=["AF3", "T7", "Pz", "T8", "AF4"], buffer_size=50, update_interval=1):
        self.ws = ws
        self.cols = []
        self.cols_to_graph = {
            col: -1
            for col in cols_to_graph
        }
        self.index = -1
        self.buffer_size = 50
        self.col_buffers = {
            col: np.full(self.buffer_size, 4075.0)
            for col in cols_to_graph
        }
        self.update_interval = update_interval
        self.fig, self.axs, self.lines = None, None, None

    def initalize(self):
        meta_data = self.ws.recv()
        meta_data = json.loads(meta_data)
        meta_data = meta_data["result"]["success"][0]
        self.cols = meta_data["cols"]
        for col in self.cols_to_graph.keys():
            if col in self.cols:
                self.cols_to_graph[col] = self.cols.index(col)

    def start_animation(self):
        self.fig, self.axs = plt.subplots(len(self.cols_to_graph), 1)
        self.lines = [ax.plot(np.full(self.buffer_size, 4075.0))[0]
                      for ax in self.axs]
        ani = animation.FuncAnimation(self.fig, self.animate,
                                      interval=self.update_interval)
        plt.show()

    def animate(self, _):
        self.index += 1
        if self.index > self.buffer_size-1:
            self.index = 0
        data = json.loads(self.ws.recv())
        data = data["eeg"]

        for i, (col, buffer) in enumerate(self.col_buffers.items()):
            buffer[self.index] = data[self.cols_to_graph[col]]
            # self.axs[i].relim()
            # self.axs[i].autoscale_view()
            # self.axs[i].lines.pop(0)
            self.lines[i].set_data(range(len(buffer)), buffer)
            # self.axs[i].plot(buffer, color='blue')

        # print(self.col_buffers)


def main():
    client = Cortex(user, debug_mode=True)
    client.do_prepare_steps()
    client.sub_request(['eeg'])
    grapher = Grapher(client.ws)
    grapher.initalize()
    print(grapher.cols)
    print(grapher.cols_to_graph)
    grapher.start_animation()


if __name__ == '__main__':
    main()
