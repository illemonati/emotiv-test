from cortex import Cortex
from config import user
import json
import datetime
import numpy as np
import os
import pandas as pd
from pathlib import Path


def main():

    seconds = float(input("Record length in seconds: "))
    print(f"recording for {seconds} seconds")

    client = Cortex(user, debug_mode=False)
    client.do_prepare_steps()
    client.sub_request(['eeg'])
    meta_data = json.loads(client.ws.recv())
    # print(meta_data)
    cols = meta_data["result"]["success"][0]["cols"]
    print(cols)

    start_time = json.loads(client.ws.recv())["time"]
    datas = []
    while True:
        data = json.loads(client.ws.recv())
        datas.append(data["eeg"])
        if data["time"] >= start_time + seconds:
            break

    df = pd.DataFrame(datas, columns=cols)

    file_path = Path(f"data/data-{start_time}-{seconds}-seconds.csv")
    Path(os.path.dirname(file_path)).mkdir(parents=True, exist_ok=True)

    df.to_csv(file_path)


if __name__ == '__main__':
    main()
